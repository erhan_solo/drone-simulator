# drone-simulator

> This project can simulate a fleet of drones that randomly `fly`/`recharge`/`stay idle` within a country.

### Why

Since there needs to be built the company's drone monitoring dashboard, this project allows us to quickly mock and simulate a fleet of drones that will integrate with our backend services.

The simulator starts a number of drones and randomly selects the next status of each drone. Statuses can be:

- Flying
- Idle (ready for new commands)
- Recharging

### Usage

- `npm run info` - run this command to check all the available commands and their documentation;
- `npm run start` - this command starts the simulator based on the default configurations;

Other useful commands include

- `npm run test` - run unit tests
- `npm run cov` - see unit tests coverage
- `npm run doc` - see project documentation generated with `typedoc`

### Simulator Configuration

ATM, the configuration can only be changed from `index.js` file since this project is meant to be a lightweight simulator and thus there aren't any configuration modules installed.

Below, the simulator's configuration parameters alongside their default values. (can be found in `src/simulator_config.ts`)

```typescript
interface DroneSpecs {
  readonly minRechargeDuration: number;
  readonly maxRechargeDuration: number;
  readonly minCapableSpeed: number;
  readonly maxCapableSpeed: number;
}
interface DroneStatusChances {
  fly: number;
  idle: number;
  recharge: number;
}
// Simulator Configuration object
interface SimulatorConfig {
  // in which country to generate the locations
  // of the drones?
  readonly country: CountryBoundaries;

  // how is the drone's activity divided between fly, idle and recharge?
  // eg. fly 30% of time, recharge 30% of time and stay idle 40%
  readonly droneStatusChances: DroneStatusChances;

  // where to send drone updates?
  readonly droneUpdatesEndpoint: string;

  // what is the interval @which the drone should updates its state
  // and send updates? duration in seconds
  readonly droneUpdatesInterval: number;

  // what is the duration at which the drone scheduler should run?
  // seconds
  readonly schedulerInterval: number;

  // how many drones to spawn?
  readonly instancesCount: number;

  // what should be the parameters that give drone capabilities
  // when generating each drone?
  readonly specs: DroneSpecs;
}

// default configuration
const defaultConfig: SimulatorConfig = {
  country: ROMANIA,
  droneStatusChances: {
    fly: 30, // fly 30% of time
    idle: 50, // stay idle 50% of time
    recharge: 20 // recharge 20% of time
  },
  droneUpdatesEndpoint: '',
  droneUpdatesInterval: 3, // seconds

  instancesCount: 15,

  schedulerInterval: 5, // seconds

  // specs determine the speed capabilities and recharge
  // durations for each drone;
  // the actual capable speed and recharge duration is
  // generated based on below boundaries
  // capable speed: random number between min/max speed
  // recharge duration: random number between min/max recharge
  // duration
  specs: {
    maxCapableSpeed: 30, // meters/second
    maxRechargeDuration: 20, // seconds
    minCapableSpeed: 5, // meters/second
    minRechargeDuration: 5 // seconds
  }
};
```

### Important considerations and future enhancements

1. The `Drone` and the `Simulator` components should be decoupled from the `Ticker` component.

`Drone` and `Simulator` rely on `Ticker` component for (simulated) real-time updates. Both `drones` and the `simulator` use the `ticker` component to either update their state or for scheduling new commands to give to drones at fixed time intervals.

However, coupling the above components directly to the implementation of the `ticker` component means that unit tests will have to work with its implementation. Since the `ticker` component uses a simple `setInterval` as its' implementation, the unit tests will have to use various timers to test for certain conditions.

Thus, `ticker`'s interface and implementation needs to be separated so that we have a different, more stable implementation when unit testing so that we get rid of proper timing within tests.

2. The communication with the central server must happen via performant RPC frameworks.

ATM, each drone will communicate with the central server via `http POST` requests that will send data in `JSON format`. However, in production we should rely on more network optimised tech like for example [Google's grpc](https://grpc.io/).

> Note that when switching to a rpc framework, the drone should send only diffs from previous updates sent so that the data transferred is even more optimised

### Project Information

This project is based on the awesome [typescript-starter](https://github.com/bitjson/typescript-starter) library. I just had to write the code and everything else was already set. Some nice features include:

- Integrated unit tests with `ava`
- Integrated coverage reporting
- Automated linting and formatting via `tslint` and `prettier`
- Auto API Docs via `typedoc`
- Auto check for dependencies and vulnerabilities via `nsp`

### License

MIT.
