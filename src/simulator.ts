import axios from 'axios';
import { omit } from 'lodash';
import {
  diffMiddleware,
  Drone,
  DroneState,
  DroneStatus,
  genRandGeoLocation,
  genRandGeoLocationIn,
  getRandBetween,
  Ticker
} from './lib';
import { addDefaultValues, SimulatorConfig } from './simulator_config';
import { DroneStatusGenerator } from './simulator_util';

// max distance of the generated random geolocation
// to which the drone will fly;
const maxFlyRadius = 1500;

interface UDroneState extends DroneState {
  // add uuid information to drone's state
  uuid: string;
}

export interface DroneInstance {
  readonly drone: Drone;
  readonly uuid: string;
}

export class Simulator {
  private readonly simulatorConfig: SimulatorConfig;
  private readonly droneStatusGenerator: DroneStatusGenerator;
  private droneInstances: DroneInstance[] = [];
  private scheduler: Ticker;

  constructor(cfg: SimulatorConfig) {
    if (!cfg.droneUpdatesEndpoint) {
      throw new Error('the endpoint for sending drone updates is needed');
    }

    this.simulatorConfig = addDefaultValues(cfg);
    this.droneStatusGenerator = new DroneStatusGenerator(
      this.simulatorConfig.droneStatusChances
    );
    this.init();
  }

  public run(): Simulator {
    this.droneInstances.forEach(d => d.drone.start());
    this.scheduler.start();
    return this;
  }

  private init(): void {
    this.makeDrones();

    this.scheduler = new Ticker(() => {
      this.scheduleDrones();
    }, this.simulatorConfig.schedulerInterval);
  }

  private makeDrones(): void {
    for (let i = 0; i < this.simulatorConfig.instancesCount; i++) {
      const uuid = `Drone_${i + 1}`;
      const d = new Drone({
        currentLocation: genRandGeoLocationIn(this.simulatorConfig.country),
        notify: (s: DroneState) => {
          this.sendDroneUpdates({
            uuid,
            ...s
          });
        },
        spec: {
          capableSpeed: getRandBetween(
            this.simulatorConfig.specs.minCapableSpeed,
            this.simulatorConfig.specs.maxCapableSpeed
          ),
          rechargeTime: getRandBetween(
            this.simulatorConfig.specs.minRechargeDuration,
            this.simulatorConfig.specs.maxRechargeDuration
          )
        },
        updatesInterval: this.simulatorConfig.droneUpdatesInterval
      });

      // apply diff middleware so that the drone state received represents only
      // differences from the previous state
      d.apply(diffMiddleware());
      this.droneInstances.push({
        drone: d,
        uuid
      });
    }
  }

  private async sendDroneUpdates(state: UDroneState): Promise<void> {
    const endpoint = `${this.simulatorConfig.droneUpdatesEndpoint}/${
      state.uuid
    }`;

    const data: any = {
      ...omit({ ...state }, 'uuid')
    };

    const res = await axios.patch(endpoint, data);

    if (res.status >= 200 && res.status < 300) {
      return;
    }

    throw new Error(
      `Couldn't send updates; Got status: ${res.status} ${res.statusText}`
    );
  }

  private scheduleDrone(d: DroneInstance): void {
    const droneState = d.drone.getState();

    if (droneState.status !== DroneStatus.Ready) {
      return;
    }

    const nextDroneStatus = this.droneStatusGenerator.getNextState();

    switch (nextDroneStatus) {
      case DroneStatus.Flying:
        const nextLoc = genRandGeoLocation(
          droneState.currentLocation,
          this.simulatorConfig.country,
          maxFlyRadius
        );
        d.drone.fly(nextLoc);
        break;
      case DroneStatus.Recharging:
        d.drone.recharge();
        break;
    }
  }

  private scheduleDrones(): void {
    this.droneInstances
      .filter(d => d.drone.getState().status === DroneStatus.Ready)
      .forEach(d => this.scheduleDrone(d));
  }
}
