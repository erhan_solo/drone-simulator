import { defaultsDeep } from 'lodash';
import { CountryBoundaries, ROMANIA } from './lib';
import { DroneStatusChances } from './simulator_util';

const defaultConfig: SimulatorConfig = {
  // what is the country where the drones will
  // be generated in?
  country: ROMANIA,

  // what is the time distribution of each drone?
  droneStatusChances: {
    fly: 20, // fly 20% of time
    idle: 60, // stay idle 50% of time
    recharge: 20 // recharge 20% of time
  },

  // http endpoint where the drone updates will be sent
  droneUpdatesEndpoint: '',

  // interval at which each drone will update its' state
  // and send them
  droneUpdatesInterval: 3, // s

  // number of drones to spawn
  instancesCount: 15,

  // time interval at which drones are given new potential commands
  // eg. fly or recharge
  schedulerInterval: 5, // s

  // specs determine the speed capabilities and recharge
  // durations for each drone;
  // the actual capable speed and recharge duration is
  // generated based on below boundaries
  // capable speed: random number between min/max speed
  // recharge duration: random number between min/max recharge
  // duration
  specs: {
    maxCapableSpeed: 30, // m/s
    maxRechargeDuration: 20, // s
    minCapableSpeed: 5, // m/s
    minRechargeDuration: 5
  }
};

export interface DroneSpecs {
  readonly minRechargeDuration: number;
  readonly maxRechargeDuration: number;
  readonly minCapableSpeed: number;
  readonly maxCapableSpeed: number;
}

export interface SimulatorConfig {
  // in which country to generate the locations
  // of the drones?
  readonly country: CountryBoundaries;

  // how is the drone's activity divided between fly, idle and recharge?
  // eg. fly 30% of time, recharge 30% of time and stay idle 40%
  readonly droneStatusChances: DroneStatusChances;

  // where to send drone updates?
  readonly droneUpdatesEndpoint: string;

  // what is the interval @which the drone should updates its state
  // and send updates? duration in seconds
  readonly droneUpdatesInterval: number;

  // what is the duration at which the drone scheduler should run?
  // seconds
  readonly schedulerInterval: number;

  // how many drones to spawn?
  readonly instancesCount: number;

  // what should be the parameters that give drone capabilities
  // when generating each drone?
  readonly specs: DroneSpecs;
}

export function addDefaultValues(c: SimulatorConfig): SimulatorConfig {
  const copy: SimulatorConfig = JSON.parse(JSON.stringify(c));
  return defaultsDeep(copy, defaultConfig);
}
