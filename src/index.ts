import { noop } from 'lodash';
import { Simulator } from './simulator';

// tslint:disable-next-line:no-console
const log = console.log.bind(console);

const never = () => {
  const fakeInterval = 100000;
  setInterval(noop, fakeInterval);
};

function main(): void {
  const simulatorConfig: any = {
    droneUpdatesEndpoint: 'http://localhost:8080/drone',
    instancesCount: 25
  };

  const simulator = new Simulator(simulatorConfig);
  simulator.run();
  log('simulator started');
  return never();
}

main();
