import { DroneStatus } from './lib/drone';
import { getRandBetween } from './lib/rand_util';

interface ChanceSegment {
  from: number;
  to: number;
  status: DroneStatus;
}

export interface DroneStatusChances {
  fly: number;
  idle: number;
  recharge: number;
}

export class DroneStatusGenerator {
  private readonly statusChanceSegments: ChanceSegment[];

  constructor(droneStatusChances: DroneStatusChances) {
    const normalizedChances = this.normalizeChances(droneStatusChances);
    this.statusChanceSegments = this.makeSegments(normalizedChances);
  }

  public getNextState(): DroneStatus {
    const rand = getRandBetween(0, 100);
    const picked = this.statusChanceSegments.find(
      segment => segment.from <= rand && segment.to > rand
    );
    return picked.status;
  }

  private makeSegments(c: DroneStatusChances): ChanceSegment[] {
    const fly: ChanceSegment = {
      from: 100 - c.fly,
      status: DroneStatus.Flying,
      to: 100
    };
    const idle: ChanceSegment = {
      from: fly.from - c.idle,
      status: DroneStatus.Ready,
      to: fly.from
    };
    const recharge: ChanceSegment = {
      from: idle.from - c.recharge,
      status: DroneStatus.Recharging,
      to: idle.from
    };

    return [recharge, idle, fly];
  }

  private normalizeChances(c: DroneStatusChances): DroneStatusChances {
    const sum = c.fly + c.idle + c.recharge;
    return {
      fly: (c.fly * 100) / sum,
      idle: (c.idle * 100) / sum,
      recharge: (c.recharge * 100) / sum
    };
  }
}
