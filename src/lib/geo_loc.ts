type meters = number;

export const KM = 1000;

export interface CountryBoundaries {
  readonly west: GeoLocation;
  readonly north: GeoLocation;
  readonly east: GeoLocation;
  readonly south: GeoLocation;
}

export class GeoLocation {
  constructor(public readonly lat: number, public readonly lng: number) {}

  /**
   * This method computes the distance in meters between the current
   * geo-coordinates to the given geo-coordinates;
   *
   * ### Description
   * This procedure uses [harvesine formula](https://en.wikipedia.org/wiki/Haversine_formula) shamelessly copied from: https://stackoverflow.com/a/34486089/2135910
   *
   * ### Example
   * ```typescript
   * const bucharest = new GeoLocation(44.4, 26.08333);
   * const budapest = new GeoLocation(47.49833, 19.04083);
   * const distance = bucharest.getDistance(budapest); //
   * ```
   *
   * @param to the geo-point to which to calculate the distance
   */
  public getDistance(to: GeoLocation): meters {
    const p = 0.017453292519943295; // Math.PI / 180
    const cos = Math.cos;
    const a =
      0.5 -
      cos((to.lat - this.lat) * p) / 2 +
      (cos(this.lat * p) *
        cos(to.lat * p) *
        (1 - cos((to.lng - this.lng) * p))) /
        2;
    const R = 6371; // km (earth's radius)
    const distance = 2 * R * Math.asin(Math.sqrt(a)); // in km

    return Math.floor(distance * 1000);
  }

  /**
   * check if this geo-location is in the specified country boundaries
   * @param boundaries country's boundaries
   */
  public inCountry(boundaries: CountryBoundaries): boolean {
    const ns =
      this.lat >= boundaries.south.lat && this.lat <= boundaries.north.lat;
    const we =
      this.lng >= boundaries.west.lng && this.lng <= boundaries.east.lng;

    return ns && we;
  }

  /**
   * isIdentical checks if 2 geo-points are the same;
   */
  public isIdentical(loc: GeoLocation): boolean {
    return loc.lat === this.lat && loc.lng === this.lng;
  }
}

/**
 * hasSignificantChanges checks wether there's a difference between the given 2 geolocation
 * points;
 *
 * ### Example
 *
 * ```typescript
 * hasSignificantChanges(undefined, undefined); // false
 * hasSignificantChanges(null, null); // false
 * hasSignificantChanges(undefined, null); // false
 * hasSignificantChanges(bucharest, bucharest); // false
 * hasSignificantChanges(bucharest, undefined); // true
 * hasSignificantChanges(bucharest, paris); // true
 * ```
 */
export function hasSignificantChanges(
  from: GeoLocation,
  to: GeoLocation,
  toleranceLevel: meters = 1
): boolean {
  // undefined -> undefined
  // null -> null
  if (!from && !to) {
    return false;
  }

  // loc -> undefined || loc -> null
  // undefined -> loc || null -> loc
  if ((!!from && !to) || (!!to && !from)) {
    return true;
  }

  // loc -> loc
  if (from.isIdentical(to)) {
    return false;
  }

  return from.getDistance(to) > toleranceLevel; // distance bigger than 1 meter;
}

/**
 * getTravelInfo returns the travel information from point A to B split in
 * waypoints based on travel speed and monitoring interval;
 *
 * @returns tuple with totalTravelTime and travel waypoints per given window interval
 */
export function getTravelInfo(
  from: GeoLocation,
  to: GeoLocation,
  speed: number, // m/s
  windowInterval: number // seconds
): [number, GeoLocation[]] {
  const distance = from.getDistance(to); // m
  const totalTime = distance / speed;
  const waypointsLen = Math.ceil(totalTime / windowInterval);

  const latDelta = to.lat - from.lat;
  const lngDelta = to.lng - from.lng;

  const waypoints: GeoLocation[] = [];
  for (let i = 0; i < waypointsLen; i++) {
    const lat = from.lat + (latDelta / waypointsLen) * (i + 1);
    const lng = from.lng + (lngDelta / waypointsLen) * (i + 1);
    waypoints.push(new GeoLocation(lat, lng));
  }

  return [totalTime, waypoints];
}
