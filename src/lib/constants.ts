import { CountryBoundaries, GeoLocation } from './geo_loc';

/**
 * This file should export any useful, reusable constants
 */

export const ROMANIA: CountryBoundaries = {
  east: new GeoLocation(45.15, 29.683333),
  north: new GeoLocation(48.25, 26.7),
  south: new GeoLocation(43.616667, 25.383333),
  west: new GeoLocation(46.116667, 20.25)
};

export const Bucharest = new GeoLocation(44.4, 26.08333);
