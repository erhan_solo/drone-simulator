import { GeoLocation, getTravelInfo } from './geo_loc';
import { Ticker } from './ticker';

export type NotifierFnSignature = (u: DroneState) => void;
export type Middleware = (u: DroneState) => DroneState;

export enum DroneStatus {
  Ready = 'Ready',
  Flying = 'Flying',
  Recharging = 'Recharging',
  Stopped = 'Stopped'
}

export interface DroneSpecs {
  readonly capableSpeed: number; // m/s
  readonly rechargeTime: number; // seconds
}

export interface DroneState {
  readonly status?: DroneStatus;
  readonly currentLocation?: GeoLocation;
  readonly destination?: GeoLocation;
  readonly currentSpeed?: number;
}

export interface DroneConfig {
  readonly spec: DroneSpecs;
  readonly currentLocation: GeoLocation;
  readonly notify: NotifierFnSignature; // the function to call with drone updates;
  readonly updatesInterval: number; // interval at which the drone should send updates, in seconds
}

export class Drone {
  private status = DroneStatus.Stopped;

  private readonly spec: DroneSpecs;
  private currentLocation: GeoLocation;
  private destination: GeoLocation = null;
  private currentSpeed = 0;

  private notifier: NotifierFnSignature;
  private middlewares: Middleware[] = [];
  private notifyStreamer: Ticker;
  private updatesInterval: number;

  constructor(cfg: DroneConfig) {
    this.spec = cfg.spec;
    this.currentLocation = cfg.currentLocation;
    this.notifier = cfg.notify;
    this.updatesInterval = cfg.updatesInterval;
    this.notifyStreamer = new Ticker(
      this.sendUpdates.bind(this),
      cfg.updatesInterval
    );
  }

  /**
   * query drone's current state
   */
  public getState(): DroneState {
    const updates: DroneState = {
      currentLocation: this.currentLocation,
      currentSpeed: this.currentSpeed,
      destination: this.destination,
      status: this.status
    };

    return updates;
  }

  /**
   * apply a middleware wrapper to the notifier fn
   *
   * ### Usage Example
   *
   * ```typescript
   * const dr = new Drone;
   * dr.apply(updates => {
   *    // make sure you don't mutate the updates object;
   *    return Object.assign({...updates}, {currentSpeed: updates.currentSpeed+10});
   * });
   * ```
   */
  public apply(mid: Middleware): Drone {
    this.middlewares.push(mid);
    return this;
  }

  /**
   * starts the drone; It changes its' status from
   * Stopped -> Ready;
   */
  public start(): void {
    if (this.status !== DroneStatus.Stopped) {
      return;
    }

    this.status = DroneStatus.Ready;
    this.sendUpdates();
    this.notifyStreamer.start();
  }

  /**
   * stops the drone; It changes its' status from
   * Ready -> Stopped;
   */
  public stop(): void {
    if (this.status !== DroneStatus.Ready) {
      // normally, the stop should also be able to
      // stop the flying to destination, and when restarting
      // to be able to continue to destination;
      // for now, we'll keep things simple
      return;
    }

    this.status = DroneStatus.Stopped;
    this.notifyStreamer.stop();
    this.sendUpdates();
  }

  /**
   * given the drone's status is Ready, it makes the drone
   * simulate a fly to the given destination;
   */
  public fly(destination: GeoLocation): void {
    if (this.status !== DroneStatus.Ready) {
      return;
    }
    this.notifyStreamer.stop();

    this.status = DroneStatus.Flying;
    this.destination = destination;
    this.currentSpeed = this.spec.capableSpeed;
    this.sendUpdates();

    const [, travelWaypoints] = getTravelInfo(
      this.currentLocation,
      destination,
      this.spec.capableSpeed,
      this.updatesInterval
    );

    new Ticker((times, stop) => {
      if (times - 1 === travelWaypoints.length) {
        stop();
        this.getReady();
        return;
      }

      const reachedWaypoint = travelWaypoints[times - 1];
      this.currentLocation = reachedWaypoint;
      this.sendUpdates();
    }, this.updatesInterval).start();
  }

  /**
   * sets the drone's status to Recharging; It will recharge
   * for the configured amount of time and then it will automatically
   * get ready
   */
  public recharge(): void {
    if (this.status !== DroneStatus.Ready) {
      return;
    }

    this.notifyStreamer.stop();
    this.status = DroneStatus.Recharging;
    this.sendUpdates();
    this.notifyStreamer.start();

    setTimeout(this.getReady.bind(this), this.spec.rechargeTime * 1000);
  }

  private sendUpdates(): void {
    const updates = this.getState();

    const updatesToSend = this.middlewares.reduce((prevUpdates, mid) => {
      return mid(prevUpdates);
    }, updates);

    this.notifier(updatesToSend);
  }

  private getReady(): void {
    this.notifyStreamer.stop();

    if (this.destination !== null) {
      this.currentLocation = this.destination;
    }

    this.destination = null;
    this.currentSpeed = 0;
    this.status = DroneStatus.Ready;
    this.sendUpdates();
    this.notifyStreamer.start();
  }
}
