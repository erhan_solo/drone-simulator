type Action = (times: number, stop: () => void) => void;

/**
 * Ticker is a helper class that can be used to call
 * a fn @interval;
 *
 * ### Usage Example
 *
 * ```typescript
 * const myTicker = new Ticker(times => console.log(`fn called ${times} times`), 1000);
 * myTicker.start();
 *
 * setTimeout(() => myTicker.stop(), 3.5);
 * // output:
 * //    -> fn called 1 times
 * //    -> fn called 2 times
 * //    -> fn called 3 times
 * ```
 *
 * @param action function which will be called at each interval
 * @param interval interval in seconds at which the action will be invoked
 */
export class Ticker {
  private activeTimer: NodeJS.Timer = null;

  constructor(
    private readonly action: Action,
    private readonly interval: number // in seconds
  ) {}

  /**
   * start the ticker
   */
  public start(): Ticker {
    if (this.activeTimer !== null) {
      return this;
    }

    let times = 1;
    this.activeTimer = setInterval(() => {
      this.action(times++, () => this.stop());
    }, this.interval * 1000);
    return this;
  }

  /**
   * stop the ticker
   */
  public stop(): Ticker {
    clearInterval(this.activeTimer);
    this.activeTimer = null;
    return this;
  }
}
