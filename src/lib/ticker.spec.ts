import { default as test } from 'ava';
import { Ticker } from './ticker';

test('start -> stop', async t => {
  let accum = 0;
  const ticker = new Ticker((times, stop) => {
    accum = times;
    if (times === 4) {
      stop();
    }
  }, 0.03);

  await new Promise(resolve => {
    ticker.start();
    setTimeout(() => {
      t.deepEqual(accum, 4);
      resolve();
    }, 140);
  });
});

test("starting twice the ticker doesn't do anything", async t => {
  let accum = 0;
  const ticker = new Ticker((times, stop) => {
    accum = times;
    if (times === 4) {
      stop();
    }
  }, 0.03);

  await new Promise(resolve => {
    ticker.start();
    setTimeout(() => {
      ticker.start();
    }, 50);

    setTimeout(() => {
      t.deepEqual(accum, 4);
      resolve();
    }, 140);
  });
});
