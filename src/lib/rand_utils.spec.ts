import { default as test } from 'ava';
import { Bucharest, ROMANIA } from './constants';
import {
  genRandGeoLocation,
  genRandGeoLocationIn,
  getRandBetween
} from './rand_util';

test('getRandBetween returns random number between given limits', async t => {
  for (let i = 0; i < 10; i++) {
    const rn = getRandBetween(1, 10);
    t.true(rn >= 1);
    t.true(rn < 10);
  }
});

test('genRandGeoLocation generates a random point at the given distance from origin', async t => {
  for (let i = 0; i < 10; i++) {
    const randDistance = getRandBetween(100, 1000);
    const randLoc = genRandGeoLocation(Bucharest, ROMANIA, randDistance);
    t.true(Bucharest.getDistance(randLoc) < randDistance);
    t.true(randLoc.inCountry(ROMANIA));
  }
});

test('genRandGeoLocationIn generates a random geolocation within the given CountryBoundaries', async t => {
  for (let i = 0; i < 10; i++) {
    const randGeoPt = genRandGeoLocationIn(ROMANIA);
    t.true(randGeoPt.inCountry(ROMANIA));
  }
});
