import { default as test } from 'ava';
import { DroneState, DroneStatus } from './drone';
import { diffMiddleware } from './drone_mid';
import { GeoLocation } from './geo_loc';

test('diff middleware propagates only differences from one state to the next one', async t => {
  const mid = diffMiddleware();
  const initialState: DroneState = {
    currentLocation: new GeoLocation(0, 0),
    currentSpeed: 100,
    destination: new GeoLocation(44.4, 26.08333),
    status: DroneStatus.Flying
  };

  t.deepEqual(mid(initialState), initialState);

  const intermediary = {
    ...initialState,
    currentLocation: new GeoLocation(1, 1),
    currentSpeed: 120
  };

  t.deepEqual(mid(intermediary), {
    currentLocation: new GeoLocation(1, 1),
    currentSpeed: 120,
    destination: undefined,
    status: undefined
  });

  t.deepEqual(mid(intermediary), {
    currentLocation: undefined,
    currentSpeed: undefined,
    destination: undefined,
    status: undefined
  });

  const final: DroneState = {
    currentLocation: new GeoLocation(44.4, 26.08333),
    currentSpeed: 0,
    destination: null,
    status: DroneStatus.Ready
  };

  t.deepEqual(mid(final), {
    currentLocation: new GeoLocation(44.4, 26.08333),
    currentSpeed: 0,
    destination: null,
    status: DroneStatus.Ready
  });
});
