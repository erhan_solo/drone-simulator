import { default as test } from 'ava';
import { Bucharest, ROMANIA } from './constants';
import {
  GeoLocation,
  getTravelInfo,
  hasSignificantChanges,
  KM
} from './geo_loc';

test('constructor', async t => {
  const bucharest = new GeoLocation(Bucharest.lat, Bucharest.lng);
  t.deepEqual(bucharest.lat, Bucharest.lat);
  t.deepEqual(bucharest.lng, Bucharest.lng);
});

test('getDistance', async t => {
  const budapest = new GeoLocation(47.49833, 19.04083);
  const distance = Bucharest.getDistance(budapest);
  const expectedDistance = 643936; // 643.9346 KM
  t.deepEqual(distance, expectedDistance);
  t.deepEqual(Bucharest.getDistance(Bucharest), 0);
});

test('inCountry bucharest -> romania', async t => {
  t.deepEqual(Bucharest.inCountry(ROMANIA), true);
});

test('inCountry tulcea -> romania', async t => {
  const tulcea = new GeoLocation(45.171616, 28.791444);
  t.deepEqual(tulcea.inCountry(ROMANIA), true);
});

test('inCountry budapest -> romania', async t => {
  const budapest = new GeoLocation(47.49833, 19.04083);
  t.deepEqual(budapest.inCountry(ROMANIA), false);
});

test('isIdentical returns true for geo-points with same coordinates', async t => {
  const bucharestCopy = new GeoLocation(44.4, 26.08333);
  t.true(
    Bucharest.isIdentical(bucharestCopy),
    'points with same coordinates should have been identical'
  );
});

test('isIdentical returns false for geo-points with different coordinates', async t => {
  const Budapest = new GeoLocation(47.49833, 19.04083);
  t.false(
    Bucharest.isIdentical(Budapest),
    'Bucharest and Budapest are different locations, therefore not identical'
  );
});

test('hasSignificantChanges returns false if the 2 geo-points are undefined or null', async t => {
  t.false(hasSignificantChanges(undefined, undefined));
  t.false(hasSignificantChanges(null, null));
  t.false(hasSignificantChanges(undefined, null));
  t.false(hasSignificantChanges(null, undefined));
});

test('hasSignificantChanges returns false if the 2 points have a negligible distance', async t => {
  const prettyCloseToBucharest = new GeoLocation(44.4, 26.08338);

  t.true(prettyCloseToBucharest.getDistance(Bucharest) < 10);

  t.false(hasSignificantChanges(Bucharest, prettyCloseToBucharest, 10));
});

test('getTravel info returns travel information for a given route', async t => {
  const ploiesti = new GeoLocation(44.938698, 26.03945);

  const speed = (72 * KM) / 3600; // convert 60 km/h -> m/s
  const minute = 60;

  t.deepEqual(Bucharest.getDistance(ploiesti), 60 * KM);

  const [totalTravelTime, travelSegments] = getTravelInfo(
    Bucharest,
    ploiesti,
    speed,
    minute
  );

  t.deepEqual(totalTravelTime, 50 * minute);
  t.deepEqual(travelSegments.length, 50);

  t.true(travelSegments[travelSegments.length - 1].isIdentical(ploiesti));

  const expectedDistance = travelSegments[1].getDistance(travelSegments[0]);
  const acceptedTolerance = 5; // accepted 5 meters deviation from the expectedDistance

  // check that the distance between consecutive points is roughly
  // the above expectedDistance (+- 5 meters)
  for (let i = 0; i < travelSegments.length; i++) {
    if (i === 0) {
      continue;
    }

    const prev = travelSegments[i - 1];
    const cur = travelSegments[i];

    // check that the distance between the 2 points is bigger than accepted tolerance level;
    // in this test should be roughly 1.2KM
    t.true(hasSignificantChanges(cur, prev, acceptedTolerance));
    // check that the distance between these 2 points and the expected distance is within
    // the accepted tolerance level;
    t.true(
      Math.abs(cur.getDistance(prev) - expectedDistance) < acceptedTolerance
    );
  }
});
