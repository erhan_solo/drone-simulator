import { CountryBoundaries, GeoLocation } from './geo_loc';

/**
 * get a runder number between the specified limits - [min:max)
 */
export function getRandBetween(min: number, max: number): number {
  return min + Math.random() * (max - min);
}

/**
 * genRandGeoLocation generates a geopoint at distance of max 1km from
 * given geo point; It makes sure the generated point is in given country
 * boundaries;
 */
export function genRandGeoLocation(
  from: GeoLocation,
  cb: CountryBoundaries,
  maxRadius: number = 1000
): GeoLocation {
  const earth = 6378137;

  const getRandomCoordinates = (radius: number) => {
    // Generate two random numbers
    let a = Math.random();
    let b = Math.random();

    // Flip for more uniformity.
    if (b < a) {
      const c = b;
      b = a;
      a = c;
    }

    return {
      east: b * radius * Math.sin((2 * Math.PI * a) / b),
      north: b * radius * Math.cos((2 * Math.PI * a) / b)
    };
  };

  const randomCoordinates = getRandomCoordinates(maxRadius);

  // Offsets in meters.
  const northOffset = randomCoordinates.north;
  const eastOffset = randomCoordinates.east;

  // Offset coordinates in radians.
  const offsetLatitude = northOffset / earth;
  const offsetLongitude =
    eastOffset / (earth * Math.cos(Math.PI * (from.lat / 180)));

  const randGeoPoint = new GeoLocation(
    from.lat + offsetLatitude * (180 / Math.PI),
    from.lng + offsetLongitude * (180 / Math.PI)
  );

  return randGeoPoint.inCountry(cb)
    ? randGeoPoint
    : genRandGeoLocation(from, cb);
}

/**
 * generate a new random geo point in the given country boundaries;
 *
 */
export function genRandGeoLocationIn(
  boundaries: CountryBoundaries
): GeoLocation {
  const randLat = getRandBetween(boundaries.south.lat, boundaries.north.lat);
  const randLng = getRandBetween(boundaries.east.lng, boundaries.west.lng);

  return new GeoLocation(randLat, randLng);
}
