import { default as test } from 'ava';
import { Bucharest, ROMANIA } from './constants';
import { Drone, DroneState, DroneStatus } from './drone';
import { GeoLocation, hasSignificantChanges } from './geo_loc';
import { genRandGeoLocation } from './rand_util';

test('apply middleware that returns null', async t => {
  const d = new Drone({
    currentLocation: new GeoLocation(0, 0),
    notify: u => {
      t.deepEqual(u, null);
    },
    spec: {
      capableSpeed: 10,
      rechargeTime: 15
    },
    updatesInterval: 3
  });

  d.apply(() => null);
  d.start();
  d.stop();
});

test('apply middleware that alters current speed', async t => {
  const d = new Drone({
    currentLocation: new GeoLocation(0, 0),
    notify: u => {
      t.deepEqual(u.currentSpeed, 15);
    },
    spec: {
      capableSpeed: 10,
      rechargeTime: 15
    },
    updatesInterval: 3
  });
  d.apply(u => ({ ...u, currentSpeed: 15 }));
  d.start();
  d.stop();
});

test('apply middleware that modifies the drone updates twice', async t => {
  const d = new Drone({
    currentLocation: new GeoLocation(0, 0),
    notify: u => {
      t.deepEqual(u.currentSpeed, 15);
      t.deepEqual(u.status, DroneStatus.Recharging);
    },
    spec: {
      capableSpeed: 10,
      rechargeTime: 15
    },
    updatesInterval: 3
  });
  d.apply(u => ({ ...u, currentSpeed: 15 })).apply(u => ({
    ...u,
    status: DroneStatus.Recharging
  }));
  d.start();
  d.stop();
});

test('start emits status update', async t => {
  const d = new Drone({
    currentLocation: new GeoLocation(0, 0),
    notify: (() => {
      let times = 1;
      return u => {
        times === 1
          ? t.deepEqual(u.status, DroneStatus.Ready)
          : t.deepEqual(u.status, DroneStatus.Stopped);
        times++;
      };
    })(),
    spec: {
      capableSpeed: 10,
      rechargeTime: 15
    },
    updatesInterval: 3
  });

  d.start();
  d.stop();
});

test('start emits status update @ interval', async t => {
  const d = new Drone({
    currentLocation: new GeoLocation(0, 0),
    notify: (() => {
      let times = 1;
      return u => {
        times < 3
          ? t.deepEqual(u.status, DroneStatus.Ready)
          : t.deepEqual(u.status, DroneStatus.Stopped);
        times++;
      };
    })(),
    spec: {
      capableSpeed: 10,
      rechargeTime: 15
    },
    updatesInterval: 0.1
  });

  await new Promise(resolve => {
    d.start();

    setTimeout(() => {
      d.stop();
      resolve();
    }, 190);
  });
});

test('stop stops the drone', async t => {
  const d = new Drone({
    currentLocation: new GeoLocation(0, 0),
    notify: (() => {
      let stopped = false;
      return (u: DroneState) => {
        switch (u.status) {
          case DroneStatus.Ready:
            if (stopped) {
              t.fail("drone already stopped, so status should've been stopped");
            }
            break;
          case DroneStatus.Stopped:
            stopped = true;
            t.deepEqual(u.currentSpeed, 0);
            t.deepEqual(u.destination, null);
            t.truthy(
              u.currentLocation.isIdentical(new GeoLocation(0, 0)),
              'it should have the same geo-location as the initial one'
            );
            break;
          default:
            if (stopped) {
              t.fail("drone already stopped, so status should've been stopped");
            }
        }
      };
    })(),
    spec: {
      capableSpeed: 10,
      rechargeTime: 15
    },
    updatesInterval: 0.1
  });

  await new Promise(resolve => {
    d.start();

    setTimeout(() => {
      d.stop();
      resolve();
    }, 300);
  });
});

test('recharge should simulate recharging of the drone', async t => {
  let rechargeEmitCount = 0;
  const d = new Drone({
    currentLocation: new GeoLocation(0, 0),
    notify: u => {
      if (u.status === DroneStatus.Recharging) {
        rechargeEmitCount++;
        t.truthy(u.currentLocation.isIdentical(new GeoLocation(0, 0)));
        t.deepEqual(u.destination, null);
        t.deepEqual(u.currentSpeed, 0);
      }
    },
    spec: {
      capableSpeed: 10,
      rechargeTime: 0.25
    },
    updatesInterval: 0.1
  });

  await new Promise(resolve => {
    d.start();
    d.recharge();
    setTimeout(() => {
      d.stop();
      const expectedRechargeEmitCount = 3; // when recharging starts and 2 within 220 ms (1 per 100ms)
      t.deepEqual(rechargeEmitCount, expectedRechargeEmitCount);
      resolve();
    }, 300);
  });
});

test('fly simulation', async t => {
  const destination = genRandGeoLocation(Bucharest, ROMANIA, 500); // a random location at 500 meters from the center of Bucharest
  // 0.5 seconds to reach the destination
  // 1000 m/s for a distance of 500m;
  const flyDuration = 500;

  let lastDroneLocation;
  const d = new Drone({
    currentLocation: Bucharest,
    notify: ((dest: GeoLocation) => {
      return (u: DroneState) => {
        if (u.status !== DroneStatus.Flying) {
          return;
        }

        t.deepEqual(u.currentSpeed, 1000);
        t.deepEqual(u.destination, dest);

        if (lastDroneLocation === undefined) {
          lastDroneLocation = u.currentLocation;
          return;
        }
        t.true(hasSignificantChanges(lastDroneLocation, u.currentLocation));
        lastDroneLocation = u.currentLocation;
      };
    })(destination),
    spec: {
      capableSpeed: 1000, // ie. 3600 km/h; it's crazy, I know :)
      rechargeTime: 5
    },
    updatesInterval: 0.03
  });

  await new Promise(resolve => {
    d.start();
    d.fly(destination);
    t.deepEqual(d.getState().status, DroneStatus.Flying);

    setTimeout(() => {
      t.deepEqual(d.getState().status, DroneStatus.Ready);
      t.false(
        hasSignificantChanges(lastDroneLocation, destination),
        `latest drone location should be its actual destination but got a distance of ${lastDroneLocation.getDistance(
          destination
        )} meters`
      );
      d.stop();
      resolve();
    }, flyDuration + 50 /* add 50 ms for safety */);
  });
});
