import { DroneState, Middleware } from './drone';
import { hasSignificantChanges } from './geo_loc';

/**
 * diffMiddleware creates a drone updates middleware which propagates only
 * the diff between the current drone updates and previously sent drone updates
 */
export const diffMiddleware: () => Middleware = () => {
  let lastUpdates: DroneState = null;
  return (u: DroneState) => {
    if (lastUpdates === null) {
      lastUpdates = u;
      return u;
    }

    const diff: DroneState = {
      currentLocation: hasSignificantChanges(
        lastUpdates.currentLocation,
        u.currentLocation
      )
        ? u.currentLocation
        : undefined,
      currentSpeed:
        lastUpdates.currentSpeed === u.currentSpeed
          ? undefined
          : u.currentSpeed,
      destination: hasSignificantChanges(lastUpdates.destination, u.destination)
        ? u.destination
        : undefined,
      status: lastUpdates.status === u.status ? undefined : u.status
    };

    lastUpdates = u;
    return diff;
  };
};
